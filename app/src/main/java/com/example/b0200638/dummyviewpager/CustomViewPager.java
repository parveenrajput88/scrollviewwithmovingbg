package com.example.b0200638.dummyviewpager;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;


public class CustomViewPager  extends ViewPager{

    private Bitmap bmp;

        private int imageHeight;
        private float overlapLevel;
        private int currentPosition = -1;
        private float currentOffset = 0.0f;
        private Rect src = new Rect();
        private Rect dst = new Rect();

        private final static String TAG = CustomViewPager.class.getSimpleName();

        public CustomViewPager(Context context) {
            super(context);
        }

        public CustomViewPager(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public void setBitmap(Bitmap bmp){
            this.bmp = bmp;
        }

        @Override
        protected void onLayout(boolean changed, int l, int t, int r, int b) {
            super.onLayout(changed, l, t, r, b);
        }


        @Override
        protected void onPageScrolled(int position, float offset, int offsetPixels) {
            super.onPageScrolled(position, offset, offsetPixels);
            currentPosition = position;
            currentOffset = offset;
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
                if (currentPosition == -1)
                    currentPosition = getCurrentItem();
                src.set((int) (overlapLevel * (currentPosition + currentOffset)), 0,
                        (int) (overlapLevel * (currentPosition + currentOffset) + getWidth()), canvas.getHeight());

                dst.set((getScrollX()), 0, (getScrollX() + canvas.getWidth()), canvas.getHeight());

                canvas.drawBitmap(bmp, src, dst, null);
        }

        public void setOverLapLevel(float overlapLevel) {
            this.overlapLevel = overlapLevel;
        }


        @Override
        public void setCurrentItem(int item) {
            super.setCurrentItem(item);
            currentPosition = item;
        }

}
