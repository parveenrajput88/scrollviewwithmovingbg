package com.example.b0200638.dummyviewpager;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class SlidePagerAdapter extends PagerAdapter{
    private int numPage;
    private Context context;

    public SlidePagerAdapter(Context context, int numpage) {
        this.numPage = numpage;
        this.context = context;
    }


    @Override
    public int getCount() {
        return numPage;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.fragment_screen_page, null);
        TextView num = (TextView) view.findViewById(R.id.tv);
        String pos = "Page " + (position + 1);
        num.setText(pos);

        container.addView(view);

        return view;
    }
}
