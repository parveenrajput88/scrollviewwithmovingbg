package com.example.b0200638.dummyviewpager;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private ImageView image;
    private Bitmap bitmap;
    private float height;
    private float width;
    private PagerAdapter mPagerAdapter;
    private CustomViewPager viewPager;
    private int numberPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        Bitmap bMap = BitmapFactory.decodeResource(getResources(), R.drawable.img_sport);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        bitmap = scaleToFitHeight(bMap, (int)height);
        image = (ImageView) findViewById(R.id.imageView);
        image.setImageBitmap(bitmap);

        float overlap = (float)(bitmap.getWidth()-width)/(float)(numberPage-1);

        mPagerAdapter = new SlidePagerAdapter(this, numberPage);
        viewPager = (CustomViewPager) findViewById(R.id.pager);
        viewPager.setBitmap(bitmap);
        viewPager.setOverLapLevel(overlap);
        viewPager.setAdapter(mPagerAdapter);

        ShowDialog();

    }


    private void ShowDialog(){
        Builder builder = new Builder(this);

        LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_enter_number, null);

        final EditText text = view.findViewById(R.id.editText);
        TextView cancelTxt = view.findViewById(R.id.tvCancel);
        TextView okTxt = view.findViewById(R.id.tvOk);

        builder.setView(view);

        final AlertDialog alert = builder.create();
        alert.setCancelable(false);
        alert.show();

        cancelTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });

        okTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Editable text1 = text.getText();
                if(TextUtils.isEmpty(text1)){
                    Toast.makeText(MainActivity.this, "Please Enter a Number", Toast.LENGTH_SHORT).show();
                } else {
                    alert.dismiss();
                    Integer page = Integer.parseInt(text1.toString());
                    numberPage = page;
                    showViewPager();

                }
            }
        });
    }

    private void showViewPager() {
        float overlap = (float)(bitmap.getWidth()-width)/(float)(numberPage-1);

        mPagerAdapter = new SlidePagerAdapter(this, numberPage);
        viewPager = (CustomViewPager) findViewById(R.id.pager);
        viewPager.setBitmap(bitmap);
        viewPager.setOverLapLevel(overlap);
        viewPager.setAdapter(mPagerAdapter);
    }

    public static Bitmap scaleToFitHeight(Bitmap b, int height)
    {
        float factor = height / (float) b.getHeight();
        return Bitmap.createScaledBitmap(b, (int) (b.getWidth() * factor), height, true);
    }
}
